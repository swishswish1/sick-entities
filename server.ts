
import bodyParser from 'body-parser';
import { install } from 'source-map-support';
import * as pkg from './package.json';
import { GraphqlServer } from './src/APIs/GraphQL';
import { RESTServer } from './src/APIs/REST';
import config from './src/common/config/config';
import HealthService from './src/common/system/components/health.service';
import { IServer } from './src/common/interfaces';
install();

const server = async (): Promise<void> => {
    await config.init(pkg.name); // TODO: use @sick/sick-config

    const restServer: IServer = new RESTServer({
        port: 8080,
        middlewares: [
            bodyParser.json({ limit: '10m' })
        ],
        dependencies: {
            config: config,
            healthService: new HealthService()
        }
    });

    restServer.start();

    const graphQLServer: IServer = new GraphqlServer({ port: 8081 });
    graphQLServer.start();
}

server();
